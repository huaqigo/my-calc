VERSION 5.00
Begin VB.Form Form1 
   BackColor       =   &H00808080&
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Form1"
   ClientHeight    =   7560
   ClientLeft      =   150
   ClientTop       =   840
   ClientWidth     =   4770
   BeginProperty Font 
      Name            =   "宋体"
      Size            =   10.5
      Charset         =   134
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "Form1.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   ScaleHeight     =   7560
   ScaleWidth      =   4770
   StartUpPosition =   3  '窗口缺省
   Begin VB.CommandButton Command34 
      Caption         =   "最小公倍数"
      BeginProperty Font 
         Name            =   "黑体"
         Size            =   9
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   1920
      TabIndex        =   34
      Top             =   3000
      Width           =   1095
   End
   Begin VB.CommandButton Command28 
      Caption         =   "最大公约数"
      BeginProperty Font 
         Name            =   "黑体"
         Size            =   9
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   1920
      TabIndex        =   33
      Top             =   3600
      Width           =   1095
   End
   Begin VB.TextBox Text1 
      Appearance      =   0  'Flat
      BeginProperty Font 
         Name            =   "宋体"
         Size            =   12
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2655
      Left            =   120
      MultiLine       =   -1  'True
      ScrollBars      =   3  'Both
      TabIndex        =   0
      Text            =   "Form1.frx":EFBD
      Top             =   120
      Width           =   4575
   End
   Begin VB.CommandButton Command33 
      Caption         =   "log"
      BeginProperty Font 
         Name            =   "黑体"
         Size            =   12
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   500
      Left            =   3120
      TabIndex        =   32
      Top             =   4200
      Width           =   700
   End
   Begin VB.CommandButton Command32 
      Caption         =   "exp"
      BeginProperty Font 
         Name            =   "黑体"
         Size            =   12
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   500
      Left            =   3960
      TabIndex        =   31
      Top             =   4200
      Width           =   700
   End
   Begin VB.CommandButton Command31 
      Caption         =   ")"
      BeginProperty Font 
         Name            =   "黑体"
         Size            =   12
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   500
      Left            =   2520
      TabIndex        =   30
      Top             =   4200
      Width           =   465
   End
   Begin VB.CommandButton Command29 
      Caption         =   "("
      BeginProperty Font 
         Name            =   "黑体"
         Size            =   12
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   500
      Left            =   1920
      TabIndex        =   29
      Top             =   4200
      Width           =   465
   End
   Begin VB.CommandButton Command30 
      Caption         =   "^"
      BeginProperty Font 
         Name            =   "黑体"
         Size            =   12
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1095
      Left            =   3960
      TabIndex        =   28
      Top             =   3000
      Width           =   700
   End
   Begin VB.CommandButton Command27 
      Caption         =   "√"
      BeginProperty Font 
         Name            =   "黑体"
         Size            =   12
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   500
      Left            =   3120
      TabIndex        =   27
      Top             =   3000
      Width           =   700
   End
   Begin VB.CommandButton Command26 
      Caption         =   "^2"
      BeginProperty Font 
         Name            =   "黑体"
         Size            =   12
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   500
      Left            =   3120
      TabIndex        =   26
      Top             =   3600
      Width           =   700
   End
   Begin VB.CommandButton Command25 
      Caption         =   "arctan"
      BeginProperty Font 
         Name            =   "黑体"
         Size            =   12
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   500
      Left            =   120
      TabIndex        =   25
      Top             =   3000
      Width           =   900
   End
   Begin VB.CommandButton Command24 
      Caption         =   "arccos"
      BeginProperty Font 
         Name            =   "黑体"
         Size            =   12
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   500
      Left            =   120
      TabIndex        =   24
      Top             =   3600
      Width           =   900
   End
   Begin VB.CommandButton Command23 
      Caption         =   "arcsin"
      BeginProperty Font 
         Name            =   "黑体"
         Size            =   12
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   500
      Left            =   120
      TabIndex        =   23
      Top             =   4200
      Width           =   900
   End
   Begin VB.CommandButton Command22 
      Caption         =   "tan"
      BeginProperty Font 
         Name            =   "黑体"
         Size            =   12
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   500
      Left            =   1080
      TabIndex        =   22
      Top             =   3000
      Width           =   700
   End
   Begin VB.CommandButton Command21 
      Caption         =   "cos"
      BeginProperty Font 
         Name            =   "黑体"
         Size            =   12
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   500
      Left            =   1080
      TabIndex        =   21
      Top             =   3600
      Width           =   700
   End
   Begin VB.CommandButton Command20 
      Caption         =   "sin"
      BeginProperty Font 
         Name            =   "黑体"
         Size            =   12
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   500
      Left            =   1080
      TabIndex        =   20
      Top             =   4200
      Width           =   700
   End
   Begin VB.CommandButton Command19 
      Caption         =   "AC"
      BeginProperty Font 
         Name            =   "黑体"
         Size            =   14.25
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   500
      Left            =   3960
      TabIndex        =   19
      Top             =   5160
      Width           =   700
   End
   Begin VB.CommandButton Command18 
      Caption         =   "DEL"
      BeginProperty Font 
         Name            =   "黑体"
         Size            =   14.25
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   500
      Left            =   3120
      TabIndex        =   18
      Top             =   5160
      Width           =   700
   End
   Begin VB.CommandButton Command17 
      Caption         =   "÷"
      BeginProperty Font 
         Name            =   "黑体"
         Size            =   14.25
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   500
      Left            =   3960
      TabIndex        =   17
      Top             =   5760
      Width           =   700
   End
   Begin VB.CommandButton Command16 
      Caption         =   "×"
      BeginProperty Font 
         Name            =   "黑体"
         Size            =   14.25
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   500
      Left            =   3120
      TabIndex        =   16
      Top             =   5760
      Width           =   700
   End
   Begin VB.CommandButton Command15 
      Caption         =   "-"
      BeginProperty Font 
         Name            =   "黑体"
         Size            =   14.25
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   500
      Left            =   3960
      TabIndex        =   15
      Top             =   6360
      Width           =   700
   End
   Begin VB.CommandButton Command14 
      Caption         =   "+"
      BeginProperty Font 
         Name            =   "黑体"
         Size            =   14.25
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   500
      Left            =   3120
      TabIndex        =   14
      Top             =   6360
      Width           =   700
   End
   Begin VB.CommandButton Command13 
      Caption         =   "="
      BeginProperty Font 
         Name            =   "黑体"
         Size            =   14.25
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   500
      Left            =   3960
      TabIndex        =   13
      Top             =   6960
      Width           =   700
   End
   Begin VB.CommandButton Command12 
      Caption         =   "Ans"
      BeginProperty Font 
         Name            =   "黑体"
         Size            =   14.25
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   500
      Left            =   3120
      TabIndex        =   12
      Top             =   6960
      Width           =   700
   End
   Begin VB.CommandButton Command11 
      Caption         =   "."
      BeginProperty Font 
         Name            =   "黑体"
         Size            =   14.25
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   500
      Left            =   2040
      TabIndex        =   11
      Top             =   6960
      Width           =   900
   End
   Begin VB.CommandButton Command1 
      Caption         =   "0"
      BeginProperty Font 
         Name            =   "黑体"
         Size            =   14.25
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   500
      Left            =   120
      TabIndex        =   10
      Top             =   6960
      Width           =   1860
   End
   Begin VB.CommandButton Command10 
      BackColor       =   &H80000005&
      Caption         =   "9"
      BeginProperty Font 
         Name            =   "黑体"
         Size            =   14.25
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   500
      Left            =   2040
      TabIndex        =   9
      Top             =   5160
      Width           =   900
   End
   Begin VB.CommandButton Command9 
      Caption         =   "8"
      BeginProperty Font 
         Name            =   "黑体"
         Size            =   14.25
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   500
      Left            =   1080
      TabIndex        =   8
      Top             =   5160
      Width           =   900
   End
   Begin VB.CommandButton Command8 
      Caption         =   "7"
      BeginProperty Font 
         Name            =   "黑体"
         Size            =   14.25
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   500
      Left            =   120
      TabIndex        =   7
      Top             =   5160
      Width           =   900
   End
   Begin VB.CommandButton Command7 
      Caption         =   "6"
      BeginProperty Font 
         Name            =   "黑体"
         Size            =   14.25
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   500
      Left            =   2040
      TabIndex        =   6
      Top             =   5760
      Width           =   900
   End
   Begin VB.CommandButton Command6 
      Caption         =   "5"
      BeginProperty Font 
         Name            =   "黑体"
         Size            =   14.25
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   500
      Left            =   1080
      TabIndex        =   5
      Top             =   5760
      Width           =   900
   End
   Begin VB.CommandButton Command5 
      Caption         =   "4"
      BeginProperty Font 
         Name            =   "黑体"
         Size            =   14.25
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   500
      Left            =   120
      TabIndex        =   4
      Top             =   5760
      Width           =   900
   End
   Begin VB.CommandButton Command4 
      Caption         =   "3"
      BeginProperty Font 
         Name            =   "黑体"
         Size            =   14.25
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   500
      Left            =   2040
      TabIndex        =   3
      Top             =   6360
      Width           =   900
   End
   Begin VB.CommandButton Command3 
      Caption         =   "2"
      BeginProperty Font 
         Name            =   "黑体"
         Size            =   14.25
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   500
      Left            =   1080
      TabIndex        =   2
      Top             =   6360
      Width           =   900
   End
   Begin VB.CommandButton Command2 
      Caption         =   "1"
      BeginProperty Font 
         Name            =   "黑体"
         Size            =   14.25
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   500
      Left            =   120
      TabIndex        =   1
      Top             =   6360
      Width           =   900
   End
   Begin VB.Menu draw 
      Caption         =   "绘制"
   End
   Begin VB.Menu history 
      Caption         =   "历史"
   End
   Begin VB.Menu about 
      Caption         =   "关于"
   End
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'=======想法========
'建立文本文件，将计算内容及结果保存到里面，历史记录功能
'手动或设置一个按钮进行清除历史记录

'=======问题========
'------------------------------------------------
' 发现问题：乘法后面的括号内计算结果不能有负号 -……-（2020/12/13）
' 哈哈哈，神来之笔，2020/12/14，解决了，实现 4×-3 这类计算，起飞
'-------------------------------------------------
' 发现问题：2+-3 程序没有报错，计算正确（2020/12/13）
' 问题 2+-3 程序运行正常的疑问解决：（2020/12/13）
' 设置一个输出语句（Text1.Text = Text1.Text & vbCrLf & "x"），
' 将之放到加减法运算函数中的不同循环体中
' 发现其运算过程为 2+0，2-3，哈哈哈，懂了
' 乘法的话就不行，乘以0就不好了
'-------------------------------------------------
' 2020/12/14/19:12:27, 发现问题，括号里面只能由基本的数字的四则运算，
' 不能有 sin 等在 = 键的过程中定义的内容，把它们放到外面？
' 2020/12/15/18:37:33 解决问题，Bracket函数的优化

'===================

Public Ans As Double

Private Sub about_Click()
    MsgBox ("huaqigo作品 | 保留所有权利" & vbCrLf _
    & "这是一个功能完善的计算器" & vbCrLf _
    & "历史记录文件路径为 App.Path" & "& " & "\history.txt" & vbCrLf _
    & "码云地址：https://gitee.com/huaqigo/my-calc" & vbCrLf _
    & "使用愉快！")
End Sub

Sub Record()
    Open App.Path & "\history.txt" For Append As #1
    Print #1, "----------------------"
    Print #1, Now
    Print #1, Text1.Text
    Close #1
End Sub

Private Sub history_Click()
    Dim str$
    Open App.Path & "\history.txt" For Input As #1
        Do Until EOF(1)
            Input #1, str
            Text1.Text = Text1.Text & str & vbCrLf
        Loop
    Close #1
End Sub

Private Sub draw_Click()
    Form2.Show
End Sub

Private Sub guessnumber()
    Dim result As Integer, guess As Integer, n As Integer
    Randomize
    result = Rnd() * 200 - 100
    
    Do
        s = InputBox("请输入您猜的数字，（输入q退出）")
        If s = "q" Then
            Exit Sub
        End If
        
        guess = Val(s)
        If guess < result Then
            Text1.Text = Text1.Text & vbCrLf & guess & Space(6) & "您猜小了"
        ElseIf guess > result Then
            Text1.Text = Text1.Text & vbCrLf & guess & Space(6) & "您猜大了"
        End If
        n = n + 1
    Loop Until guess = result
    Text1.Text = Text1.Text & vbCrLf & guess & Space(6) & "您猜对了！"
    Text1.Text = Text1.Text & vbCrLf & "一共猜了" & n & "次"
End Sub
'Private Sub text1_KeyPress(KeyAscii As Integer)     ' 按回车键进行计算
'
'    If KeyAscii = 13 Then
'        Call basic(Text1.Text)
'        Text1.Text = Text1.Text & vbCrLf & "= " & ans
'    End If
'
'End Sub

Private Sub Form_Load()
    Form1.Width = 4870
    Form1.Height = 8400
    Form1.Caption = "MyCalc"
    
    Text1.Left = Form1.ScaleLeft
    Text1.Top = Form1.ScaleTop
    Text1.Width = Form1.ScaleWidth
    Text1.Text = ""
    Text1.Locked = True
    
End Sub

Private Sub Command13_Click()                       ' = 键的程序
        
    Select Case LCase(Text1.Text)
        Case "..."
            Text1.Locked = False
            Text1.Text = ""
            Exit Sub
        Case "lock"
            Text1.Locked = True
            Text1.Text = ""
            Exit Sub
        Case "time"
            Text1.Text = Text1.Text & vbCrLf & Now
            Exit Sub
        Case "history"
            Text1.Text = Text1.Text & vbCrLf
            Call history_Click
            Exit Sub
        Case "draw"
            Text1.Text = ""
            Call draw_Click
            Exit Sub
        Case "game"
            Call guessnumber
            Exit Sub
        Case "你好"
            Text1.Text = Text1.Text & vbCrLf & "哇，你又来用我了！"
            Exit Sub
        Case "about"
            Text1.Text = Text1.Text & vbCrLf & "huaqigo作品 | 保留所有权利" & vbCrLf _
            & "这是一个功能完善的计算器" & vbCrLf _
            & "历史记录文件路径为 App.Path" & "& " & "\history.txt" & vbCrLf _
            & "码云地址：https://gitee.com/huaqigo/my-calc" & vbCrLf _
            & "使用愉快！"
            Exit Sub
    End Select
    
    
    Dim formula As String
    formula = Text1.Text
    
    formula = Bracket(formula)
    formula = Exponential(formula)
    formula = InverseTrig(formula)
    formula = Trigonometric(formula)
    formula = Logarithmic(formula)
    
    Ans = Basic(formula)
    Text1.Text = Text1.Text & vbCrLf & "= " & Ans
    
    Call Record

'============检验是否是数字==================
'    ch = Text1.Text
'    If IsNumeric(ch) Then
'
'        Text1.Text = Text1.Text & vbCrLf & ch & "是数字"
'    Else
'        Text1.Text = Text1.Text & vbCrLf & ch & "不是数字"
'    End If
'============================================
             
End Sub

'===============四则运算函数======================

Function Basic#(ByVal mystring$)
    Dim s As String, ansr As Double
    s = mystring
'=================================================
' 实现乘除法运算 2020/12/12/20:44:36，西山生活区第一学生宿舍434室，哈哈哈哈，牛批
' 马上跟加减法运算整合， 嘎嘎
' huaqigo在宿舍成功实现了四则运算，2020/12/12/20:52:40，come on!

     Do While InStr(s, "×") Or InStr(s, "÷")
     
'        If InStr(s, "×-") Then
'            Text1.Text = Text1.Text & vbCrLf & "算式中有“×-”，不讲武德，请检查并重新输入"
'            Exit Function
'        End If
        
        mu = InStr(s, "×")
        di = InStr(s, "÷")
        
        If mu < di Then                     ' flag = 0 进行乘法运算
            If mu <> 0 Then                 ' flag = 1 进行除法运算
                loca = mu                   ' loca 表示 符号的location
                flag = 0
            Else
                loca = di
                flag = 1
            End If
        ElseIf di < mu Then
            If di <> 0 Then
                loca = di
                flag = 1
            Else
                loca = mu
                flag = 0
            End If
        End If


        If IsNumeric(Mid(s, 1, loca - 1)) Then          '判断符号左边的部分是不是数字
            ansr = Val(Mid(s, 1, loca - 1))
        Else
            i = 1
            Do While IsNumeric(Mid(s, loca - i, 1)) Or Mid(s, loca - i, 1) = "."
                i = i + 1
            Loop
            ansr = Val(Mid(s, loca - i + 1, i - 1))
        End If

        j = 1
        'Do While IsNumeric(Mid(s, loca + j, 1)) Or Mid(s, loca + j, 1) = "."
        If IsNumeric(Mid(s, loca + j, 1)) Then             ' 哈哈哈，神来之笔，2020/12/14
            j = 1                                          ' 通过一个判断允许了 4×-3 这样的运算
        ElseIf IsNumeric(Mid(s, loca + j, 2)) Then
            j = 2
        End If
        
        Do While IsNumeric(Mid(s, loca + j, 1)) Or Mid(s, loca + j, 1) = "."
            j = j + 1
        Loop
        
        If flag = 0 Then                                        '乘除运算
            ansr = ansr * Val(Mid(s, loca + 1, j - 1))
        ElseIf flag = 1 Then
            If Val(Mid(s, loca + 1, j - 1)) = 0 Then            ' 2020/12/15/20:44:45 添加
                Text1.Text = Text1.Text & vbCrLf & "除数是0，请修改算式"
                Exit Function
            End If
            ansr = ansr / Val(Mid(s, loca + 1, j - 1))
        End If

        If IsNumeric(Mid(s, 1, loca - 1)) Then                '更新字符串s
            s = CStr(ansr) & Mid(s, loca + j, Len(s))
        Else
            s = Mid(s, 1, loca - i) & CStr(ansr) & Mid(s, loca + j, Len(s))
        End If
    Loop

'==========================================
' 实现加法运算 2020/12/7/11：40，哈哈哈哈，牛批

' 实现整数加减法运算，2020/12/7/14：45，哈哈哈哈，牛批
' 允许以正负号开头

    Do While Not IsNumeric(s)

        If IsNumeric(Mid(s, 1, 1)) Then         '判断开头是否合法
            i = 1
        ElseIf IsNumeric(Mid(s, 1, 2)) Then
            i = 2
        Else
            Text1.Text = Text1.Text & vbCrLf & "算式开头不合法，请检查并重新输入"
            Exit Function
        End If

'        Do While IsNumeric(Mid(s, j, 1))
        Do While IsNumeric(Mid(s, i, 1)) Or Mid(s, i, 1) = "."      '支持小数
            i = i + 1
        Loop

        ansr = Val(Mid(s, 1, i - 1))            '将开头的数字赋值给结果变量 ansr

'        i = j
'        j = j + 1
'        Do While IsNumeric(Mid(s, j, 1)) Or Mid(s, j, 1) = "."
'            j = j + 1
'        Loop

'----2020/12/15/13:44:36更新（从乘除部分复制过来的）-------
'为了解决 4--3 不能正常运算的问题
'______获取运算符后的数字的通用代码框架_____

        j = 1
        'Do While IsNumeric(Mid(s, loca + j, 1)) Or Mid(s, loca + j, 1) = "."
        If IsNumeric(Mid(s, i + j, 1)) Then             ' 哈哈哈，神来之笔，2020/12/14
            j = 1                                          ' 通过一个判断允许了 4×-3 这样的运算
        ElseIf IsNumeric(Mid(s, i + j, 2)) Then
            j = 2
        End If
        Do While IsNumeric(Mid(s, i + j, 1)) Or Mid(s, i + j, 1) = "."
            j = j + 1
        Loop
'------------------------------------------------------------
        Select Case Mid(s, i, 1)
            Case "+"
                ansr = ansr + Val(Mid(s, i + 1, j - 1))
            Case "-"
                ansr = ansr - Val(Mid(s, i + 1, j - 1))
        End Select

        s = CStr(ansr) & Mid(s, i + j, Len(s))    '简化字符串（算式）

    Loop
    
    Basic = Val(s)
    
End Function

'==========函数整理工作==========
' 项目开始时间：2020/12/15/16:23:55
' 2020/12/15/18:12:45 整理完成，代码已备份
' 准备开始解决 括号内 只能 进行四则运算的优化


'================括号=================
'2020/12/13/11:33:01 , huaqigo在宿舍实现单个括号内容的处理
'2020/12/13/11:47:11 , huaqigo在宿舍实现多重括号内容的处理，嘎嘎
Function Bracket$(ByVal mystring$)
    Dim str1 As String, str2 As String
    str1 = mystring

    Do While InStr(str1, "(")

        For n = 1 To Len(str1)
            If Mid(str1, n, 1) = "(" Then
                i = n
            End If
        Next
                                                'i存放“（”的位置，j存放“）”的位置
        j = i + 1
        Do While Not Mid(str1, j, 1) = ")"      '不用“）”也能计算
            j = j + 1
            If j = Len(str1) + 1 Then
                Exit Do
            End If
        Loop
        str2 = Mid(str1, i + 1, j - i - 1)

'-------修改，适应括号里出现 sin 等函数的状况--------
'-------2020/12/15/18:24:08 开始---------------------
'-------2020/12/15/18:37:33 完成----我得儿意的笑-----

'        If InStr(str2, "sin") Or InStr(str2, "cos") Or InStr(str2, "tan") Or InStr(str2, "log") Or InStr(str2, "exp") Then
'            Text1.Text = Text1.Text & vbCrLf & "停停停！括号里的内容太复杂了，我干不了" & vbCrLf & "您消消气哈，回去喝口茶，改改再来"
'            Exit Function
'        End If

        Do While InStr(str2, "sin") Or InStr(str2, "cos") Or InStr(str2, "tan") Or InStr(str2, "log") Or InStr(str2, "exp") Or InStr(str2, "^")
            str2 = Exponential(str2)
            str2 = InverseTrig(str2)
            str2 = Trigonometric(str2)
            str2 = Logarithmic(str2)
        Loop
'----------------------------------------------------
        str1 = Mid(str1, 1, i - 1) & CStr(Basic(str2)) & Mid(str1, j + 1, Len(str1))
    Loop
    
    Bracket = str1
End Function

'================指数=================
' 2020/12/14，八角楼204，huaqigo实现了指数部分的处理
' 2020/12/14/17:29:15，宿舍，huaqigo修正了一个括号处理的bug，
' 并完成了指数的循环，well done!
Function Exponential$(ByVal mystring$)
    Dim str As String, exponent As Single, base As Single
    str = Bracket(mystring)
    
    Do While InStr(str, "^")
        i = InStr(str, "^")                        '变量 i 存放 ^ 的位置
        If Not IsNumeric(Mid(str, i - 1, 1)) Then
            Text1.Text = Text1.Text & vbCrLf & "好汉请看：^前面不是数字，请修改算式再来过"
            Exit Function
        End If
    
        j = 1
        If IsNumeric(Mid(str, i + j, 1)) Then
            j = 1
        ElseIf IsNumeric(Mid(str, i + j, 2)) Then
            j = 2
        End If
        
        Do While IsNumeric(Mid(str, i + j, 1)) Or Mid(str, i + j, 1) = "."
            j = j + 1
        Loop
        
        exponent = Val(Mid(str, i + 1, j - 1))      '变量exponent存放指数
        
        k = 1
        Do While IsNumeric(Mid(str, i - k, 1))
             k = k + 1
             If i = k Then Exit Do
        Loop
        
        base = Val(Mid(str, i - k + 1, k - 1))
        str = Mid(str, 1, i - k) & CStr(base ^ exponent) & Mid(str, i + j, Len(str))
    Loop
    Exponential = str
End Function

'================反三角函数（角度）=================
'复制三角函数的代码，敲敲打打即可
' 2020/12/14/19:08:38 完成
Function InverseTrig$(ByVal mystring$)
    Dim str As String, flag As Integer

    str = Bracket(mystring)
    
    Do While InStr(str, "arcsin") Or InStr(str, "arccos") Or InStr(str, "arctan")
    
        If InStr(str, "arcsin") <> 0 Then
            i = InStr(str, "arcsin")
            flag = 1
        ElseIf InStr(str, "arccos") <> 0 Then
            i = InStr(str, "arccos")
            flag = 2
        ElseIf InStr(str, "arctan") Then
            i = InStr(str, "arctan")
            flag = 3
        End If
        i = i + 5
        
'______获取运算符后的数字的通用代码框架_____
        j = 1
        If IsNumeric(Mid(str, i + j, 1)) Then
            j = 1
        ElseIf IsNumeric(Mid(str, i + j, 2)) Then          '使得 负号 合法
            j = 2
        End If
        
        Do While IsNumeric(Mid(str, i + j, 1)) Or Mid(str, i + j, 1) = "."
            j = j + 1
        Loop
'______________________________________
        num = Val(Mid(str, i + 1, j - 1)) * 3.1416 / 180   '角度转弧度
        Select Case flag
            Case 1
                str = Mid(str, 1, i - 6) & CStr(Format(1 / Sin(num), "#0.00##")) & Mid(str, i + j, Len(str))
            Case 2
                str = Mid(str, 1, i - 6) & CStr(Format(1 / Cos(num), "#0.00##")) & Mid(str, i + j, Len(str))
            Case 3
                str = Mid(str, 1, i - 6) & CStr(Format(1 / Tan(num), "#0.00##")) & Mid(str, i + j, Len(str))
        End Select
    Loop
    InverseTrig = str
End Function

'================三角函数（角度）=================
' 项目开始时间：2020/12/14/17:42:38
' 项目完成时间：2020/12/14/18:32:10
' hahaha
Function Trigonometric$(ByVal mystring$)
    Dim str As String, flag As Integer
    
    str = Bracket(mystring)
    
    Do While InStr(str, "sin") Or InStr(str, "cos") Or InStr(str, "tan")
        If InStr(str, "sin") <> 0 Then
            i = InStr(str, "sin")
            flag = 1
        ElseIf InStr(str, "cos") <> 0 Then
            i = InStr(str, "cos")
            flag = 2
        ElseIf InStr(str, "tan") Then
            i = InStr(str, "tan")
            flag = 3
        End If
        i = i + 2
        
'______获取运算符后的数字的通用代码框架_____
        j = 1
        If IsNumeric(Mid(str, i + j, 1)) Then
            j = 1
        ElseIf IsNumeric(Mid(str, i + j, 2)) Then          '使得 负号 合法
            j = 2
        End If
        
        Do While IsNumeric(Mid(str, i + j, 1)) Or Mid(str, i + j, 1) = "."
            j = j + 1
        Loop
'______________________________________
        num = Val(Mid(str, i + 1, j - 1)) * 3.1416 / 180   '角度转弧度
'        Select Case flag
'            Case 1
'                str = Mid(str, 1, i - 3) & CStr(Sin(num)) & Mid(str, i + j, Len(str))
'            Case 2
'                str = Mid(str, 1, i - 3) & CStr(Cos(num)) & Mid(str, i + j, Len(str))
'            Case 3
'                str = Mid(str, 1, i - 3) & CStr(Tan(num)) & Mid(str, i + j, Len(str))
'        End Select
        Select Case flag
            Case 1
                str = Mid(str, 1, i - 3) & CStr(Format(Sin(num), "#0.00##")) & Mid(str, i + j, Len(str))
            Case 2
                str = Mid(str, 1, i - 3) & CStr(Format(Cos(num), "#0.00##")) & Mid(str, i + j, Len(str))
            Case 3
                str = Mid(str, 1, i - 3) & CStr(Format(Tan(num), "#0.00##")) & Mid(str, i + j, Len(str))
        End Select
    Loop
    
    Trigonometric = str
End Function

'================log和exp函数=================
' 项目开始时间：2020/12/14/19:14:35
' 项目结束时间：2020/12/14/19:40:01
' 说明：此处的 log 是以e为底的，换底 除以 log(base) ，exp也是以e为底的
Function Logarithmic$(ByVal mystring)
    Dim str As String, num As Single, flag As Integer
    str = Bracket(mystring)
    
    Do While InStr(str, "log") Or InStr(str, "exp")
        If InStr(str, "log") <> 0 Then
            i = InStr(str, "log")
            flag = 1
        ElseIf InStr(str, "exp") <> 0 Then
            i = InStr(str, "exp")
            flag = 2
        End If
        i = i + 2
        
'______获取运算符后的数字的通用代码框架_____
        j = 1
        If IsNumeric(Mid(str, i + j, 1)) Then
            j = 1
        ElseIf IsNumeric(Mid(str, i + j, 2)) Then          '使得 负号 合法
            j = 2
        End If
        
        Do While IsNumeric(Mid(str, i + j, 1)) Or Mid(str, i + j, 1) = "."
            j = j + 1
        Loop
'______________________________________
        num = Val(Mid(str, i + 1, j - 1))
        Select Case flag
            Case 1
                str = Mid(str, 1, i - 3) & CStr(Format(Log(num), "#0.00##")) & Mid(str, i + j, Len(str))
            Case 2
                str = Mid(str, 1, i - 3) & CStr(Format(Exp(num), "#0.00##")) & Mid(str, i + j, Len(str))
        End Select
        
    Loop
    Logarithmic = str
End Function

Function ZZXCF%(ByVal m%, ByVal n%)

    If m < n Then
        t = m: m = n: n = t
    End If
    
    r = m Mod n
    Do Until r = 0
        m = n
        n = r
        r = m Mod n
    Loop
    ZZXCF = n
End Function

Private Sub Command28_Click()
    Dim n1 As Integer, n2 As Integer
    n1 = Val(InputBox("请输入第一个正整数"))
    n2 = Val(InputBox("请输入第二个正整数"))
    
    Text1.Text = Text1.Text & vbCrLf & n1 & "和" & n2 & "的最大公约数是：" & ZZXCF(n1, n2)
    Call Record
    
End Sub

Private Sub Command34_Click()
    Dim n1 As Integer, n2 As Integer
    n1 = Val(InputBox("请输入第一个正整数"))
    n2 = Val(InputBox("请输入第二个正整数"))
    
    Text1.Text = Text1.Text & vbCrLf & n1 & "和" & n2 & "的最小公倍数是：" & n1 * n2 / ZZXCF(n1, n2)
    Call Record
    
End Sub


Private Sub Command32_Click()
    Text1.Text = Text1.Text & "exp("
End Sub

Private Sub Command33_Click()
    Text1.Text = Text1.Text & "log("
End Sub

Private Sub Command23_Click()
    Text1.Text = Text1.Text & "arcsin("
End Sub

Private Sub Command24_Click()
    Text1.Text = Text1.Text & "arccos("
End Sub

Private Sub Command25_Click()
    Text1.Text = Text1.Text & "arctan("
End Sub

Private Sub Command20_Click()
    Text1.Text = Text1.Text & "sin("
End Sub

Private Sub Command21_Click()
    Text1.Text = Text1.Text & "cos("
End Sub

Private Sub Command22_Click()
    Text1.Text = Text1.Text & "tan("
End Sub

Private Sub Command26_Click()
    Text1.Text = Text1.Text & "^(2)"
End Sub

Private Sub Command27_Click()
    Text1.Text = Text1.Text & "^(0.5)"
End Sub

Private Sub Command30_Click()
    Text1.Text = Text1.Text & "^("
End Sub

Private Sub Command29_Click()
    Text1.Text = Text1.Text & "("
End Sub

Private Sub Command31_Click()
    Text1.Text = Text1.Text & ")"
End Sub

Private Sub Command14_Click()
    Text1.Text = Text1.Text & "+"
End Sub

Private Sub Command15_Click()
    Text1.Text = Text1.Text & "-"
End Sub

Private Sub Command16_Click()
    Text1.Text = Text1.Text & "×"
End Sub

Private Sub Command17_Click()
    Text1.Text = Text1.Text & "÷"
End Sub

Private Sub Command12_Click()           ' Ans 变量存储上一次计算结果
    Text1.Text = Text1.Text & Ans
End Sub

Private Sub Command18_Click()           'Del键，删除尾部一个字符
'    s1 = Text1.Text
'    For i = 1 To Len(s1) - 1
'        s2 = s2 & Mid(s1, i, 1)
'    Next i
'    Text1.Text = s2

    If Text1.Text <> "" Then
        Text1.Text = Mid(Text1.Text, 1, Len(Text1.Text) - 1)
    Else
        Text1.Text = ""
    End If
    
End Sub

Private Sub Command19_Click()           'AC键，清空
    Text1.Text = ""
End Sub

Private Sub Command1_Click()
    Text1.Text = Text1.Text & "0"
End Sub

Private Sub Command2_Click()
    Text1.Text = Text1.Text & "1"
End Sub

Private Sub Command3_Click()
    Text1.Text = Text1.Text & "2"
End Sub

Private Sub Command4_Click()
    Text1.Text = Text1.Text & "3"
End Sub

Private Sub Command5_Click()
    Text1.Text = Text1.Text & "4"
End Sub

Private Sub Command6_Click()
    Text1.Text = Text1.Text & "5"
End Sub

Private Sub Command7_Click()
    Text1.Text = Text1.Text & "6"
End Sub

Private Sub Command8_Click()
    Text1.Text = Text1.Text & "7"
End Sub

Private Sub Command9_Click()
    Text1.Text = Text1.Text & "8"
End Sub

Private Sub Command10_Click()
    Text1.Text = Text1.Text & "9"
End Sub

Private Sub Command11_Click()
    Text1.Text = Text1.Text & "."
End Sub

