VERSION 5.00
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "comdlg32.ocx"
Begin VB.Form Form2 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "函数图像绘制窗口"
   ClientHeight    =   9510
   ClientLeft      =   45
   ClientTop       =   390
   ClientWidth     =   14055
   Icon            =   "Form2.frx":0000
   LinkTopic       =   "Form2"
   MaxButton       =   0   'False
   ScaleHeight     =   9510
   ScaleWidth      =   14055
   StartUpPosition =   3  '窗口缺省
   Begin VB.CommandButton Command4 
      Caption         =   "保存图片"
      BeginProperty Font 
         Name            =   "宋体"
         Size            =   10.5
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   615
      Left            =   9840
      TabIndex        =   52
      Top             =   8640
      Width           =   3975
   End
   Begin VB.CommandButton Command3 
      Caption         =   "绘制"
      Height          =   495
      Left            =   12360
      TabIndex        =   50
      Top             =   7680
      Width           =   855
   End
   Begin VB.PictureBox Color3 
      BackColor       =   &H00800000&
      Height          =   255
      Left            =   12840
      ScaleHeight     =   195
      ScaleWidth      =   555
      TabIndex        =   49
      Top             =   7275
      Width           =   615
   End
   Begin VB.TextBox Text18 
      Height          =   270
      Left            =   12840
      TabIndex        =   48
      Text            =   "Text18"
      Top             =   6915
      Width           =   615
   End
   Begin VB.TextBox Text17 
      Height          =   270
      Left            =   12840
      TabIndex        =   47
      Text            =   "Text17"
      Top             =   6555
      Width           =   615
   End
   Begin VB.TextBox Text16 
      Height          =   270
      Left            =   10680
      TabIndex        =   42
      Text            =   "Text16"
      Top             =   7320
      Width           =   615
   End
   Begin VB.TextBox Text15 
      Height          =   270
      Left            =   10680
      TabIndex        =   41
      Text            =   "Text15"
      Top             =   6960
      Width           =   615
   End
   Begin VB.TextBox Text14 
      Height          =   270
      Left            =   10680
      TabIndex        =   40
      Text            =   "Text14"
      Top             =   6600
      Width           =   615
   End
   Begin VB.PictureBox Color1 
      BackColor       =   &H00000000&
      Height          =   255
      Left            =   11160
      ScaleHeight     =   195
      ScaleWidth      =   555
      TabIndex        =   34
      Top             =   1995
      Width           =   615
   End
   Begin MSComDlg.CommonDialog CommonDialog1 
      Left            =   9000
      Top             =   240
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin VB.PictureBox Color2 
      BackColor       =   &H00000080&
      Height          =   255
      Left            =   12720
      ScaleHeight     =   195
      ScaleWidth      =   555
      TabIndex        =   32
      Top             =   4395
      Width           =   615
   End
   Begin VB.TextBox Text13 
      Height          =   270
      Left            =   12720
      TabIndex        =   30
      Text            =   "Text13"
      Top             =   4035
      Width           =   615
   End
   Begin VB.TextBox Text12 
      Height          =   270
      Left            =   12720
      TabIndex        =   28
      Text            =   "Text12"
      Top             =   3675
      Width           =   615
   End
   Begin VB.CommandButton Command2 
      Caption         =   "绘制"
      Height          =   495
      Left            =   12360
      TabIndex        =   26
      Top             =   4920
      Width           =   855
   End
   Begin VB.TextBox Text11 
      Height          =   270
      Left            =   10680
      TabIndex        =   25
      Text            =   "Text11"
      Top             =   5115
      Width           =   615
   End
   Begin VB.TextBox Text10 
      Height          =   270
      Left            =   10680
      TabIndex        =   24
      Text            =   "Text10"
      Top             =   4755
      Width           =   615
   End
   Begin VB.TextBox Text9 
      Height          =   270
      Left            =   10680
      TabIndex        =   23
      Text            =   "Text9"
      Top             =   4395
      Width           =   615
   End
   Begin VB.TextBox Text8 
      Height          =   270
      Left            =   10680
      TabIndex        =   22
      Text            =   "Text8"
      Top             =   4035
      Width           =   615
   End
   Begin VB.TextBox Text7 
      Height          =   270
      Left            =   10680
      TabIndex        =   21
      Text            =   "Text7"
      Top             =   3675
      Width           =   615
   End
   Begin VB.CommandButton Command1 
      Caption         =   "重置坐标"
      Height          =   495
      Left            =   12360
      TabIndex        =   14
      Top             =   1800
      Width           =   855
   End
   Begin VB.TextBox Text6 
      Height          =   270
      Left            =   11160
      TabIndex        =   13
      Text            =   "Text6"
      Top             =   1635
      Width           =   615
   End
   Begin VB.TextBox Text5 
      Height          =   270
      Left            =   11160
      TabIndex        =   11
      Text            =   "Text5"
      Top             =   1275
      Width           =   615
   End
   Begin VB.TextBox Text4 
      Height          =   270
      Left            =   12120
      TabIndex        =   7
      Text            =   "Text4"
      Top             =   915
      Width           =   615
   End
   Begin VB.TextBox Text3 
      Height          =   270
      Left            =   11160
      TabIndex        =   6
      Text            =   "Text3"
      Top             =   915
      Width           =   615
   End
   Begin VB.TextBox Text2 
      Height          =   270
      Left            =   12120
      TabIndex        =   5
      Text            =   "Text2"
      Top             =   555
      Width           =   615
   End
   Begin VB.TextBox Text1 
      Height          =   270
      Left            =   11160
      TabIndex        =   4
      Text            =   "Text1"
      Top             =   555
      Width           =   615
   End
   Begin VB.PictureBox Pic 
      ForeColor       =   &H00C00000&
      Height          =   9375
      Left            =   120
      ScaleHeight     =   9315
      ScaleWidth      =   9435
      TabIndex        =   0
      Top             =   0
      Width           =   9495
   End
   Begin VB.Label Label27 
      Alignment       =   2  'Center
      Caption         =   "三角函数"
      BeginProperty Font 
         Name            =   "宋体"
         Size            =   10.5
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   10080
      TabIndex        =   51
      Top             =   5760
      Width           =   1095
   End
   Begin VB.Shape Shape3 
      BorderColor     =   &H00808080&
      Height          =   2415
      Left            =   9840
      Top             =   5880
      Width           =   3975
   End
   Begin VB.Label Label26 
      Caption         =   "点的颜色"
      Height          =   255
      Left            =   11880
      TabIndex        =   46
      Top             =   7320
      Width           =   735
   End
   Begin VB.Label Label25 
      Caption         =   "点的间距"
      Height          =   255
      Left            =   11880
      TabIndex        =   45
      Top             =   6960
      Width           =   735
   End
   Begin VB.Label Label24 
      Caption         =   "点的宽度"
      Height          =   255
      Left            =   11880
      TabIndex        =   44
      Top             =   6600
      Width           =   735
   End
   Begin VB.Label Label23 
      Caption         =   "×pi"
      Height          =   255
      Left            =   11340
      TabIndex        =   43
      Top             =   7365
      Width           =   615
   End
   Begin VB.Label Label22 
      Caption         =   "T = "
      Height          =   255
      Left            =   10200
      TabIndex        =   39
      Top             =   7320
      Width           =   615
   End
   Begin VB.Label Label21 
      Caption         =   "ω = "
      Height          =   255
      Left            =   10140
      TabIndex        =   38
      Top             =   6960
      Width           =   615
   End
   Begin VB.Label Label20 
      Caption         =   "A = "
      Height          =   255
      Left            =   10200
      TabIndex        =   37
      Top             =   6600
      Width           =   615
   End
   Begin VB.Label Label19 
      Caption         =   "y = A * Sin(ωx + T)"
      BeginProperty Font 
         Name            =   "宋体"
         Size            =   10.5
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   10080
      TabIndex        =   36
      Top             =   6120
      Width           =   2295
   End
   Begin VB.Label Label18 
      Alignment       =   2  'Center
      Caption         =   "多项式函数"
      BeginProperty Font 
         Name            =   "宋体"
         Size            =   10.5
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   10080
      TabIndex        =   35
      Top             =   2640
      Width           =   1215
   End
   Begin VB.Label Label17 
      Caption         =   "轴线颜色"
      Height          =   255
      Left            =   10200
      TabIndex        =   33
      Top             =   2040
      Width           =   735
   End
   Begin VB.Label Label16 
      Caption         =   "点的颜色"
      Height          =   255
      Left            =   11880
      TabIndex        =   31
      Top             =   4440
      Width           =   735
   End
   Begin VB.Label Label15 
      Caption         =   "点的间距"
      Height          =   255
      Left            =   11880
      TabIndex        =   29
      Top             =   4080
      Width           =   735
   End
   Begin VB.Label Label14 
      Caption         =   "点的宽度"
      Height          =   255
      Left            =   11880
      TabIndex        =   27
      Top             =   3720
      Width           =   735
   End
   Begin VB.Label Label13 
      Caption         =   "E = "
      Height          =   255
      Left            =   10200
      TabIndex        =   20
      Top             =   5160
      Width           =   615
   End
   Begin VB.Label Label12 
      Caption         =   "D = "
      Height          =   255
      Left            =   10200
      TabIndex        =   19
      Top             =   4800
      Width           =   615
   End
   Begin VB.Label Label11 
      Caption         =   "C = "
      Height          =   255
      Left            =   10200
      TabIndex        =   18
      Top             =   4440
      Width           =   615
   End
   Begin VB.Label Label10 
      Caption         =   "B = "
      Height          =   255
      Left            =   10200
      TabIndex        =   17
      Top             =   4080
      Width           =   615
   End
   Begin VB.Label Label9 
      Caption         =   "A = "
      Height          =   255
      Left            =   10200
      TabIndex        =   16
      Top             =   3720
      Width           =   615
   End
   Begin VB.Label Label8 
      Caption         =   "y = Ax^4 + Bx^3 + Cx^2 + Dx + E"
      BeginProperty Font 
         Name            =   "宋体"
         Size            =   10.5
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   10080
      TabIndex        =   15
      Top             =   3120
      Width           =   3495
   End
   Begin VB.Label Label7 
      Caption         =   "轴线宽度"
      Height          =   255
      Left            =   10200
      TabIndex        =   12
      Top             =   1680
      Width           =   735
   End
   Begin VB.Label Label6 
      Caption         =   "单位长度"
      Height          =   255
      Left            =   10200
      TabIndex        =   10
      Top             =   1320
      Width           =   735
   End
   Begin VB.Label Label5 
      Caption         =   "-"
      Height          =   255
      Left            =   11880
      TabIndex        =   9
      Top             =   960
      Width           =   255
   End
   Begin VB.Label Label4 
      Caption         =   "-"
      Height          =   255
      Left            =   11880
      TabIndex        =   8
      Top             =   600
      Width           =   255
   End
   Begin VB.Label Label3 
      Caption         =   "Y 的范围"
      Height          =   255
      Left            =   10200
      TabIndex        =   3
      Top             =   960
      Width           =   855
   End
   Begin VB.Label Label2 
      Caption         =   "X 的范围"
      Height          =   255
      Left            =   10200
      TabIndex        =   2
      Top             =   600
      Width           =   855
   End
   Begin VB.Label Label1 
      Alignment       =   2  'Center
      Caption         =   "设定坐标系"
      BeginProperty Font 
         Name            =   "宋体"
         Size            =   12
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   10080
      TabIndex        =   1
      Top             =   120
      Width           =   1335
   End
   Begin VB.Shape Shape1 
      BorderColor     =   &H00808080&
      Height          =   2175
      Left            =   9840
      Top             =   240
      Width           =   3975
   End
   Begin VB.Shape Shape2 
      BorderColor     =   &H00808080&
      Height          =   2775
      Left            =   9840
      Top             =   2760
      Width           =   3975
   End
End
Attribute VB_Name = "Form2"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
' 绘图窗口的代码于2020/12/15/14:55:31 基本完成
' 2020/12/17/17:51:38，
' 修复部分缺陷
' 增加保存图片功能


'====================窗体初始化====================

Private Sub Form_Load()
    Form2.AutoRedraw = True
    Form2.Width = 14100
    Form2.Height = 10000

    Pic.Left = Form2.ScaleLeft
    Pic.Top = Form2.ScaleTop
    Pic.Height = Form2.ScaleHeight
    Pic.Width = Form2.ScaleHeight
    Pic.AutoRedraw = True

    Text1.Text = "-10"
    Text2.Text = "10"
    Text3.Text = "-10"
    Text4.Text = "10"
    Text5.Text = "1"
    Text6.Text = "1"
    
    Text7.Text = "0"
    Text8.Text = "0"
    Text9.Text = "0"
    Text10.Text = "0"
    Text11.Text = "1"
    Text12.Text = "1"
    Text13.Text = "0.01"
    
    Text14.Text = "1"
    Text15.Text = "1"
    Text16.Text = "0"
    Text17.Text = "1"
    Text18.Text = "0.01"
    
    Call Command1_Click


'    Text7.Text = "-10"
'    Text8.Text = "10"
'    Text9.Text = "-10"
'    Text10.Text = "10"
'
End Sub

'====================重置坐标系====================

Private Sub Command1_Click()
    Dim x1!, x2!, y1!, y2!, span!, lw!
    
    x1 = Val(Text1.Text)
    x2 = Val(Text2.Text)
    y1 = Val(Text3.Text)
    y2 = Val(Text4.Text)
    span = Val(Text5.Text)
    lw = Val(Text6.Text)
    
    
    Pic.Cls
    Pic.Scale (x1 - 1, y2 + 1)-(x2 + 1, y1 - 1)
    Pic.DrawWidth = lw
    Pic.ForeColor = Color1.BackColor
    
    Pic.Line (x1 - 0.5, 0)-(x2 + 0.5, 0)
    Pic.Line (0, y1 - 0.5)-(0, y2 + 0.5)

    Pic.CurrentX = x2 + 0.5: Pic.CurrentY = -0.5: Pic.Print "X"     ' 坐标轴标记
    Pic.CurrentX = 0.5: Pic.CurrentY = y2 + 0.5: Pic.Print "Y"

    For i = x1 To x2 Step span
        Pic.Line (i, 0)-(i, 0.1)
        Pic.CurrentX = i - 0.2: Pic.CurrentY = -0.1: Pic.Print i
    Next i

    For i = y1 To y2 Step span
        Pic.Line (0, i)-(0.1, i)
        Pic.CurrentX = -0.7: Pic.CurrentY = i + 0.3: Pic.Print i
    Next i

End Sub

'====================绘制多项式函数====================

Private Sub Command2_Click()
    Dim va!, vb!, vc!, vd!, ve!
    Dim x1!, x2!, y1!, y2!, span!, lw!
    
    x1 = Val(Text1.Text)
    x2 = Val(Text2.Text)
    y1 = Val(Text3.Text)
    y2 = Val(Text4.Text)
    span = Val(Text13.Text)
    lw = Val(Text12.Text)
    
    va = Val(Text7.Text)
    vb = Val(Text8.Text)
    vc = Val(Text9.Text)
    vd = Val(Text10.Text)
    ve = Val(Text11.Text)
    
    Pic.DrawWidth = lw
    Pic.ForeColor = Color2.BackColor
    
    For x = x1 To x2 Step span
        Pic.PSet (x, va * x ^ 4 + vb * x ^ 3 + vc * x ^ 2 + vd * x + ve)
    Next x
End Sub

'====================绘制三角函数====================

Private Sub Command3_Click()
    Dim va!, vw!, vt!, pi!
    Dim x1!, x2!, y1!, y2!, span!, lw!
    
    x1 = Val(Text1.Text)
    x2 = Val(Text2.Text)
    y1 = Val(Text3.Text)
    y2 = Val(Text4.Text)
    span = Val(Text18.Text)
    lw = Val(Text17.Text)
    pi = 3.14159
    
    va = Val(Text14.Text)
    vw = Val(Text15.Text)
    vt = Val(Text16.Text)
    
    Pic.DrawWidth = lw
    Pic.ForeColor = Color3.BackColor
    
    For x = x1 To x2 Step span
        Pic.PSet (x, va * Sin(vw * x + vt * pi))
    Next x
    
End Sub


'====================输出图片====================

Private Sub Command4_Click()
    On Error GoTo Cancel
    CommonDialog1.FileName = "func"
    CommonDialog1.InitDir = App.Path
    CommonDialog1.Filter = "Jpeg files(*.jpg)|*.jpg|Bmp files(*.bmp)|*.bmp"
    CommonDialog1.FilterIndex = 1
    CommonDialog1.CancelError = True
    CommonDialog1.ShowSave
    
    SavePicture Pic.Image, CommonDialog1.FileName
    Exit Sub
Cancel:

End Sub

'====================设置颜色====================

Private Sub Color1_Click()
    On Error GoTo Cancel
    CommonDialog1.CancelError = True
    CommonDialog1.ShowColor
    Color1.BackColor = CommonDialog1.Color
    Exit Sub
Cancel:
    
End Sub

Private Sub Color2_Click()
    On Error GoTo Cancel
    CommonDialog1.CancelError = True
    CommonDialog1.ShowColor
    Color2.BackColor = CommonDialog1.Color
    Exit Sub
Cancel:

End Sub

Private Sub Color3_Click()
    On Error GoTo Cancel
    CommonDialog1.CancelError = True
    CommonDialog1.Action = 3
    Color3.BackColor = CommonDialog1.Color
    Exit Sub
Cancel:
    
End Sub

